<template>
    <div class="dcy-content">
        <!-- 查询条件 -->
        <Form ref="queryParams" :model="queryParams" inline :label-width="60" label-position="left">
            <#list table.fields as field>
            <#if !field.keyFlag>
            <FormItem label="${field.comment}">
                <Input v-model="queryParams.${field.propertyName}" clearable placeholder="请输入${field.comment}"/>
            </FormItem>
            </#if>
            </#list>
            <FormItem :label-width="0" class="search-btn-mar">
                <Button icon="md-search" type="primary" @click="refresh()">查询</Button>
                <Button icon="md-refresh" type="primary" @click="reset()">重置</Button>
                <Button icon="md-add" type="primary" @click="add${cfg.modules}()">添加</Button>
                <Button icon="md-add" type="primary" @click="removeBatch()" :disabled="delBtnDisabled">删除</Button>
            </FormItem>
        </Form>

        <!-- 表格 -->
        <dcy-table
                ref="dcyTable"
                unique-id="${cfg.modulesApi}Id"
                :query-params="queryParams"
                :url="url"
                @table-select-val="selectVal"
                :columns="columns">
            <template slot-scope="{ row, index }" slot="action">
                <a @click="update(row)">修改</a>
                <Divider type="vertical"/>
                <a @click="remove(row)">删除</a>
            </template>
        </dcy-table>

        <!-- 添加表单 -->
        <Modal
                v-model="dialogShow"
                :title="dialogTitle"
                :mask-closable="false"
                @on-cancel="handleReset('form')">
            <Form ref="form" :model="form" :rules="rules" :label-width="80">
                <#list table.fields as field>
                <#if !field.keyFlag>
                <FormItem label="${field.comment}" prop="${field.propertyName}">
                    <Input v-model="form.${field.propertyName}" placeholder="请输入${field.comment}"/>
                </FormItem>
                </#if>
                </#list>
                <FormItem class="form-btn-mar">
                    <Button type="primary" @click="handleSubmit('form')">提交</Button>
                    <Button @click="handleReset('form')">重置</Button>
                </FormItem>
            </Form>
            <!-- 自定义按钮组 -->
            <div slot="footer">
                <Button type="text" size="large" @click="handleReset('form')">取消</Button>
                <Button type="primary" size="large" @click="handleSubmit('form')">确认</Button>
            </div>
        </Modal>


    </div>
</template>

<script>
    import DcyTable from "@/components/dcy/dcy-table";
    import {add${cfg.modules}, deleteBatch${cfg.modules}ById, delete${cfg.modules}ById, update${cfg.modules}} from "@/api/admin/${cfg.modulesApi}";
    import {noticeError, noticeSuccess} from "@/libs/notice";

    export default {
        name: "${cfg.modulesApi}-manage",
        components: {DcyTable},
        data() {
            return {
                url: '/admin-service/${cfg.modulesApi}/page',
                columns: [
                    <#list table.fields as field>
                    <#if !field.keyFlag>
                    {title: '${field.comment}', key: '${field.propertyName}', align: 'center'},
                    </#if>
                    </#list>
                    {title: '操作', slot: 'action', align: 'center'}
                ],
                queryParams: {},
                dialogShow: false,
                delBtnDisabled: true,
                ids: [],
                dialogTitle: '添加${cfg.modulesName}',
                form: {},
                rules: {
                    <#list table.fields as field>
                    <#if !field.keyFlag>
                    ${field.propertyName}: [this.$ruler('${field.comment}')],
                    </#if>
                    </#list>
                }
            }
        },
        methods: {
            /**
             * 刷新
             */
            refresh() {
                // 清空选中状态
                this.$refs.dcyTable.selectAll(false);
                this.$refs.dcyTable.refresh()
            },
            /**
             * 重置搜索条件
             */
            reset() {
                this.queryParams = {}
            },
            /**
             * 修改弹出框
             * @param row
             */
            update(row) {
                this.form = {...row};
                this.dialogTitle = '修改${cfg.modulesName}';
                this.dialogShow = true;
            },
            /**
             * 添加弹出框
             */
            add${cfg.modules}() {
                this.dialogTitle = '添加${cfg.modulesName}';
                this.dialogShow = true;
            },
            /**
             * 批量删除
             */
            removeBatch() {
                this.$Modal.confirm({
                    title: '您确认删除所选内容吗？',
                    onOk: () => {
                        deleteBatch${cfg.modules}ById(this.ids).then(res => {
                            if (res.data) {
                                noticeSuccess('del')
                            } else {
                                noticeError('del')
                            }
                            this.refresh()
                        })
                    }
                });
            },
            /**
             * 提交表单
             * @param name
             */
            handleSubmit(name) {
                this.$refs[name].validate((valid) => {
                    if (valid) {
                        if (this.dialogTitle === '添加用户') {
                            add${cfg.modules}(this.form).then(res => {
                                if (res.data) {
                                    noticeSuccess('add')
                                } else {
                                    noticeError('add')
                                }
                                this.cancelDialogAndRefresh()
                            })
                        } else {
                            update${cfg.modules}(this.form).then(res => {
                                if (res.data) {
                                    noticeSuccess('upd')
                                } else {
                                    noticeError('upd')
                                }
                                this.cancelDialogAndRefresh()
                            })
                        }
                    }
                })
            },
            /**
             * 行内删除
             * @param row
             */
            remove(row) {
                this.$Modal.confirm({
                    title: '您确认删除这条内容吗？',
                    onOk: () => {
                        delete${cfg.modules}ById(row.${cfg.modulesApi}Id).then(res => {
                            if (res.data) {
                                noticeSuccess('del')
                            } else {
                                noticeError('del')
                            }
                            this.refresh()
                        })
                    }
                });
            },
            /**
             * 重置表单
             * @param name
             */
            handleReset(name) {
                this.dialogShow = false;
                this.$refs[name].resetFields()
            },
            /**
             * 关闭弹出框 和 刷新表格
             */
            cancelDialogAndRefresh() {
                // 关闭弹出框 和 清空表单
                this.handleReset('form');
                // 刷新表格
                this.refresh();
            },
            /**
             * 点击每一行的checkbox
             */
            selectVal(ids) {
                this.ids = ids;
                this.delBtnDisabled = !this.ids.length;
            }
        }
    }
</script>

<style scoped>

</style>
