package com.tsq.generator.utils;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.setting.dialect.Props;
import com.tsq.generator.model.GenerModel;

public class GenUtils {
    private static final Props props;

    static {
        props = new Props("db.properties", CharsetUtil.UTF_8);
    }

    public static GenerModel getGenerByProps() {
        GenerModel generModel = new GenerModel();
        generModel.setPack(props.getStr("db.pack"));
        generModel.setDbUrl(props.getStr("db.url"));
        generModel.setDriverName(props.getStr("db.driverName"));
        generModel.setUsername(props.getStr("db.username"));
        generModel.setPassword(props.getStr("db.password"));
        generModel.setModulesName(props.getStr("db.modulesName"));
        generModel.setTableName(props.getStr("table.tableName"));
        generModel.setModules(props.getStr("table.modules"));
        return generModel;
    }
}
