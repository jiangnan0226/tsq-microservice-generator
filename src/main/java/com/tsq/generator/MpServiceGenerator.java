package com.tsq.generator;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.tsq.generator.model.GenerModel;
import com.tsq.generator.utils.GenUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：dcy
 * @Description: mybatis-plus 代码生成器  微服务使用
 * @Date: 2019/9/6 13:47
 */
public class MpServiceGenerator {

    /**
     * RUN THIS
     */
    public static void main(String[] args) {
        GenerModel generModel = GenUtils.getGenerByProps();
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        //开启 BaseResultMap
        gc.setBaseResultMap(true);
        // 主键自增
        gc.setIdType(IdType.ASSIGN_ID);
        //开启 baseColumnList
        gc.setBaseColumnList(true);
        gc.setSwagger2(true);
        gc.setFileOverride(true);

        //gc.setOutputDir(projectPath + "/mybatis-plus-sample-generator/src/main/java");
        // 生成文件的输出目录
        gc.setOutputDir(generModel.getPack());
        //开发人员
        gc.setAuthor("dcy");
        // 是否打开输出目录
        gc.setOpen(false);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(generModel.getDbUrl());
        dsc.setDriverName(generModel.getDriverName());
        dsc.setUsername(generModel.getUsername());
        dsc.setPassword(generModel.getPassword());
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.tsq");
        pc.setEntity("api.model");
        pc.setXml("mapper");
        //父包模块名
//        pc.setModuleName(models);
        mpg.setPackageInfo(pc);

        setTemplateMapper(mpg, generModel);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        // 设置父类
        strategy.setSuperControllerClass("com.tsq.web.base.controller.BaseController");
        strategy.setSuperEntityClass("com.tsq.db.base.model.BaseModel");
        strategy.setSuperServiceClass("com.tsq.web.base.service.BaseService");
        strategy.setSuperServiceImplClass("com.tsq.web.base.service.impl.BaseServiceImpl");


        strategy.setInclude(generModel.getTableName());
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setEntityColumnConstant(true);
        strategy.setRestControllerStyle(true);
        mpg.setStrategy(strategy);
        // 选择 freemarker 引擎需要指定如下加，注意 pom 依赖必须有！
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        TemplateConfig tc = new TemplateConfig();
        tc.setController("/templatesFreeMybatis/controller.java");
        tc.setService("/templatesFreeMybatis/service.java");
        tc.setServiceImpl("/templatesFreeMybatis/serviceImpl.java");
        tc.setEntity("/templatesFreeMybatis/entity.java");
        tc.setMapper("/templatesFreeMybatis/mapper.java");
        tc.setXml("/templatesFreeMybatis/mapper.xml");
        mpg.setTemplate(tc);
        mpg.execute();
    }

    private static void setTemplateMapper(AutoGenerator mpg, final GenerModel generModel) {
        // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】  ${cfg.abc}
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("modulesName", generModel.getModulesName());
                map.put("modulesApi", generModel.getModules());
                map.put("modules", generModel.getModulesUp());
                this.setMap(map);
            }
        };
        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
        focList.add(new FileOutConfig("/templatesFreeMybatis/manage-element.vue.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return generModel.getPack() + "//vue//" + generModel.getModules() + "//" + generModel.getModules() + "-manage.vue";
            }
        });
        focList.add(new FileOutConfig("/templatesFreeMybatis/vue.js.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return generModel.getPack() + "//vue//" + generModel.getModules() + "//" + generModel.getModules() + ".js";
            }
        });
        // 下划线转换驼峰
        String entityName = StringUtils.underlineToCamel(generModel.getTableName());
        final String entityNameUp = entityName.substring(0, 1).toUpperCase() + entityName.substring(1);
        focList.add(new FileOutConfig("/templatesFreeMybatis/clientService.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return generModel.getPack() + "//client-service//" + entityNameUp + "ClientService" + StringPool.DOT_JAVA;
            }
        });

        focList.add(new FileOutConfig("/templatesFreeMybatis/remoteService.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return generModel.getPack() + "//api-service//" + entityNameUp + "RemoteService" + StringPool.DOT_JAVA;
            }
        });

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);
    }


}
